import time
import cv2
import cv2.aruco
import numpy as np
import time
import traceback
import sys
import math
import cameras

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('ipd', help="ipd you think this is")
args = parser.parse_args()

np.set_printoptions(suppress = True, precision=8)

cv2.namedWindow("frame",0) # 0 at end makes it resizeable in Python 

calibration = np.load(f"cache/{args.ipd}/cameraCalibration_cv2.npz")
print(dict(calibration))

cap = cameras.MosesCaptureRectified(calibration=calibration, indices = cameras.INDEX_TRY_ALL, manexp = False)

while True:
	ret, frame = cap.read()
	if ret:
		sf = 1
		shw = cv2.resize(frame, (0, 0), fx = sf, fy = sf)
		cv2.imshow(f"frame",shw)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	

cap.release()
cv2.destroyAllWindows()