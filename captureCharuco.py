import time
import cv2
import cv2.aruco
import numpy as np
import random
import multiprocessing
import traceback
import sys
import os
import shutil
import math

import argparse
import cameras

def fx(x,y):
	xC = 1280/2
	yC = 720/2
	fac = 1.3

	num = 1 + fac*(math.sqrt( .7*(xC-x)**2 + (yC-y) **2 ) / xC)
	# print(num)

	return (num,num,num)

mulgrid = np.zeros((720,1280,3),dtype=np.float)

for i in range(1280):
	for j in range(720):
		mulgrid[j,i] = fx(i,j)

# cv2.imshow("mulgrid",mulgrid-1)
# cv2.waitKey(0)



# if not stereo, use very detailed grid. if stereo, use less detailed grid.
squaresize = 0.04105
arucosize = squaresize*.8

parser = argparse.ArgumentParser()
parser.add_argument('ipd', help="ipd you think this is")
args = parser.parse_args()

capturefolder = f"cache/{args.ipd}/cameracal/"
# print(list(os.listdir(f"cache/{args.ipd}")))
try: 
	if "cameracal" in os.listdir(f"cache/{args.ipd}"):
		os.system(f"rm -r {capturefolder}")  # shutil.rmtree(f"cache/{args.folder}/cameracal/") # warning: move your files if you don't want em deleted here
except FileNotFoundError:
	os.system(f"mkdir -p cache/{args.ipd}")


try:
	os.system(f"mkdir -p {capturefolder}")
except:
	traceback.print_exc(file=sys.stdout)

cap = cameras.MosesCaptureStereo(cameras.INDEX_FROM_JSON)

checkerboardWidth = 12
checkerboardHeight = 8

monitorWidthPx = 2560
monitorHeightPx = 1440
bine = 15

numImagesRequired = (checkerboardWidth-1)*(checkerboardHeight-1)


dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_100)
board = cv2.aruco.CharucoBoard_create(checkerboardWidth,checkerboardHeight,squaresize,arucosize,dictionary)
img = board.draw((monitorWidthPx,monitorHeightPx-(2*bine)))

img = np.vstack((np.ones((bine,monitorWidthPx)), img, np.ones((bine,monitorWidthPx))))


cv2.namedWindow("oh yeah", 0)
cv2.moveWindow("oh yeah", 2200, 100)
cv2.setWindowProperty("oh yeah", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

cv2.imshow('oh yeah',img)

#Start capturing images for calibration
cameras = []

allCorners = [[],[]]
allIds = [[],[]]

allCornersStereo = [[],[]]

doAppend = False
saveIdx = 0




while True:


	key = cv2.waitKey(1)
	if key & 0xFF == ord('q'):
		break
	ret,combinedFrame = cap.read()


	frames = {"left": combinedFrame[:, :int(combinedFrame.shape[1]/2)], "right": combinedFrame[:, int(combinedFrame.shape[1]/2):]}
	uped = False
	out = {}
	for name, frame in frames.items():
		frame = np.clip( frame.astype(np.float) * mulgrid,0,255)

		frame = frame.astype(np.uint8)
		gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

		arucoCorners, arucoIds, _ = cv2.aruco.detectMarkers(gray,dictionary)

		if len(arucoCorners)>0:
			retval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco(arucoCorners, arucoIds ,gray,board,)
			cv2.aruco.drawDetectedMarkers(frame,arucoCorners,arucoIds)
			cv2.aruco.drawDetectedCornersCharuco(frame,charucoCorners,charucoIds)

			if (key & 0xFF == ord("e")) and (charucoCorners is not None) and (len(charucoCorners) > 3):
				cv2.imwrite(f"{capturefolder}/{name}{saveIdx}.png", gray)
				print(f"saving {name}")
		out[name] = frame
	
	if (key & 0xFF == ord("e")):
		saveIdx +=1


	sf = .7
	shw = cv2.resize(np.vstack((out['left'],out['right'])), (0, 0), fx = sf, fy = sf)
	cv2.imshow(f"frame",shw)
	


	

imsize = gray.shape

cap.release()
cv2.destroyAllWindows()



# for n in range(2):
# 	idsreee = allIds[n]
# 	cornersreee = allCorners[n]
# 	l = len(idsreee)
# 	r = list(range(len(idsreee)))

# 	random.shuffle(r)

# 	newAllCorners = []
# 	newAllIds = []

# 	for ele in r:
# 		newAllCorners.append(cornersreee[ele])
# 		newAllIds.append(idsreee[ele])

# 	print("number of calibs:", l)

# 	allCorners[n] = newAllCorners[:min(numImagesRequired,l)]
# 	allIds[n] = newAllIds[:min(numImagesRequired,l)]

# 	print("now it's just", len(allCorners[n]), len(allIds[n]))

# def wrape(allCorners,allIds,board,imsize, ye, filename):
# 	# calibrateCameraCharuco has some assert()s in it, I think as sanity checks to make sure the math hasn't gone horribly wrong.
# 	# These asserts will raise an exception though, so catch it
# 	try:
# 		cameraMatrixInit = np.array([[ 1000.,    0., imsize[0]/2.],
# 		                    [    0., 1000., imsize[1]/2.],
# 		                    [    0.,    0.,           1.]])
# 		# print(cameraMatrixInit)

# 		distCoeffsInit = np.zeros((5,1))
# 		flags = (cv2.CALIB_USE_INTRINSIC_GUESS  + cv2.CALIB_FIX_ASPECT_RATIO + cv2.CALIB_RATIONAL_MODEL)
# 		(retval, cameraMatrix, distCoeffs, rvecs, 
# 		tvecs, stdDeviationsIntrinsics, stdDeviationsExtrinsics,
# 		perViewErrors) = cv2.aruco.calibrateCameraCharucoExtended(
# 												charucoCorners = allCorners, charucoIds = allIds, board = board,imageSize = imsize,cameraMatrix=cameraMatrixInit,distCoeffs=distCoeffsInit,flags=(cv2.CALIB_USE_INTRINSIC_GUESS  + cv2.CALIB_FIX_ASPECT_RATIO + cv2.CALIB_RATIONAL_MODEL),criteria=(cv2.TERM_CRITERIA_EPS & cv2.TERM_CRITERIA_COUNT, 10000, 1e-9))
# 		yeah[filename] = {"cameraMatrix": cameraMatrix, "distCoeffs": distCoeffs, "rvecs": rvecs, "tvecs": tvecs}
# 		# with open(filename, 'wb') as f:
# 		# 	# Pickle the 'data' dictionary using the highest protocol available.
# 		# 	pickle.dump((retval, cameraMatrix, distCoeffs, rvecs, tvecs), f, pickle.HIGHEST_PROTOCOL)

# 	except:
# 		traceback.print_exc(file=sys.stdout)
# 		print("oh no", filename)


# print("okay, calbrating....")
# nobs = []
# man = multiprocessing.Manager()
# yeah = man.dict()



# nobs.append(multiprocessing.Process(target=wrape, args=(allCorners[0],allIds[0],board,imsize, yeah, "left")))
# nobs.append(multiprocessing.Process(target=wrape, args=(allCorners[1],allIds[1],board,imsize, yeah, "right")))



# for job in nobs:
# 	job.start()

# jobs = nobs

# for nob in jobs:
# 	nob.join()

# print(len(allCorners[0]))


# # np.savez("cameraCalibration_cv2.npz",
# # 		leftCameraMatrix=yeah['left']['cameraMatrix'],
# # 		rightCameraMatrix=yeah['right']['cameraMatrix'],
# # 		leftDistCoeffs=yeah['left']['distCoeffs'],
# # 		rightDistCoeffs=yeah['right']['distCoeffs'],
# # 		)
# # import datetime
# # import shutil
# # timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
# # shutil.copy("cameraCalibration_cv2.npz", f"cameraCalibration_cv2-{timea}.npz")
# # exit(0)



# # Chessboard parameters
# # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ...., (checkerboardWidth, checkerboardHeight,0)
# objpp = np.zeros(((checkerboardHeight-1)*(checkerboardWidth-1),3), np.float32)
# objpp[:,:2] = np.mgrid[0:checkerboardWidth-1,0:checkerboardHeight-1].T.reshape(-1,2)
# objpp = objpp * squaresize # Set the Object Points to be in real coordinates
# objpp = np.asarray([objpp])
# objp = np.copy(objpp)
# for x in range(len(allCornersStereo[0])-1):
# 	objp = np.concatenate((objp, objpp), axis=0)
# # print(objp)

# # objp = objp
# # objp = objp[:len(allCornersStereo[0])]
# print("hello", len(objp))

# valid, leftCameraMatrix, leftDistCoeffs, rightCameraMatrix, rightDistCoeffs, leftToRightRot, leftToRightTrans, essentialMat, fundamentalMat = (
#  							cv2.stereoCalibrate(objp, allCornersStereo[0], allCornersStereo[1], yeah["left"]["cameraMatrix"], yeah["left"]["distCoeffs"], yeah["right"]["cameraMatrix"], yeah["right"]["distCoeffs"], imsize))

# if(valid):
# 	# Construct the stereo-rectified parameters for display
# 	R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = cv2.stereoRectify(leftCameraMatrix,  leftDistCoeffs, 
# 																	  rightCameraMatrix, rightDistCoeffs, 
# 																	 imsize, 
# 																	  leftToRightRot, leftToRightTrans)

# 	leftUndistortMap = [None, None]
# 	leftUndistortMap[0], leftUndistortMap[1] = cv2.initUndistortRectifyMap(leftCameraMatrix, leftDistCoeffs, 
# 																		   R1, P1, imsize, cv2.CV_32FC1)
# 	rightUndistortMap = [None, None]
# 	rightUndistortMap[0], rightUndistortMap[1] = cv2.initUndistortRectifyMap(rightCameraMatrix, rightDistCoeffs, 
# 																			 R2, P2, imsize, cv2.CV_32FC1)
# 	undistortMap = (leftUndistortMap, rightUndistortMap)

# 	print("Stereo Calibration Completed!")
# 	print("Left to Right Rotation Matrix:")
# 	print(leftToRightRot)
# 	print("Left to Right Translation:")
# 	print(leftToRightTrans)
# 	print("Essential Matrix:")
# 	print(essentialMat)
# 	print("Fundamental Matrix:")
# 	print(fundamentalMat)
# 	np.savez("cameraCalibration_cv2.npz",
# 			leftCameraMatrix=leftCameraMatrix,
# 			rightCameraMatrix=rightCameraMatrix,
# 			leftDistCoeffs=leftDistCoeffs,
# 			rightDistCoeffs=rightDistCoeffs,
# 			leftToRightTrans=leftToRightTrans,
# 			leftToRightRot=leftToRightRot,
# 			R1=R1,
# 			R2=R2,
# 			P1=P1,
# 			P2=P2,
# 			baseline=float(leftToRightTrans[0]*-1.0)
# 			)
# 	import datetime
# 	import shutil
# 	timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
# 	shutil.copy("cameraCalibration_cv2.npz", f"cameraCalibration_cv2-{timea}.npz")


	
