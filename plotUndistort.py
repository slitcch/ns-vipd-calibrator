import matplotlib.pyplot as plt
import cv2
import numpy as np
 
fig = plt.figure()
ax = fig.add_subplot(211)

calibration = np.load("cameraCalibration_cv2.npz")
 
# Make the grid
step = 20
y, x = np.meshgrid(
        np.arange(0, 720, step, dtype=np.float32),
        np.arange(0, 1280, step, dtype=np.float32)
    )
 
imgPoints = np.dstack((x.ravel(),y.ravel()))
mtx = calibration['rightCameraMatrix']
dist = calibration['rightDistCoeffs']
print(f"matrix is {mtx}")
xy_undistorted = cv2.undistortPoints(imgPoints, mtx, dist)
u,v = np.dsplit(xy_undistorted, 2)
u = u.ravel().reshape(x.shape)
v = v.ravel().reshape(x.shape)
ax.quiver(x, y, u, v)

# ax.

plt.show()