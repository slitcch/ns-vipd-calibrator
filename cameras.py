import cv2
import numpy as np
import math
import simplejson as json
import time

INDEX_FROM_JSON = 0
INDEX_TRY_ALL = 1

import abc
import typing


class Camera(metaclass=abc.ABCMeta):

	@abc.abstractmethod
	def readCapture(self) -> typing.Tuple[bool, np.ndarray]:
		return

	@abc.abstractmethod
	def release(self):
		return

	@property
	@abc.abstractmethod
	def resolution(self) -> typing.Tuple[int, int]:
		return

	@property
	@abc.abstractmethod
	def calibration(self) -> dict:
		return

	@property
	def fisheye(self):
		return False

	def readNewFrame(self, waitTime=0.1, tryCount=10):
		for _ in range(tryCount):
			ret, frame = self.readCapture()
			if(ret is True):
				return frame
			time.sleep(waitTime)
		self.release()
		raise Exception('cannot read frame')
		return None

class MosesCaptureStereo:
	frameWidth = 1280
	frameHeight = 720
	cameras = []
	def __init__(self,indices=INDEX_FROM_JSON, expval = 57, manexp = True):
		if indices == INDEX_FROM_JSON:
			j = json.load(open("cameras.json"))
			indices = [j["leftIndex"],j["rightIndex"]]
		
		if indices == INDEX_TRY_ALL:
			indices = range(10)

		for idx in indices:
			#Start capturing images for calibration
			cap = cv2.VideoCapture(idx, cv2.CAP_V4L2)
			cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
			cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
			cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
			cap.set(cv2.CAP_PROP_FPS,350)
			cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0 if manexp else 3.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
			# 70 was too low, power frequency lines.
			cap.set(cv2.CAP_PROP_EXPOSURE, expval)
			ret,frame = cap.read()
			print(ret, idx)
			if ret == True:
				self.cameras.append(cap)
	def read(self):
		ret0, f0  = self.cameras[0].read()
		ret1, f1 = self.cameras[1].read()
		ret0, f0  = self.cameras[0].read()
		ret1, f1 = self.cameras[1].read()
		while ((ret0 == False) or (ret1 == False)):
			print("no")
			ret0, f0  = self.cameras[0].read()
			ret1, f1 = self.cameras[1].read()
		f = np.hstack((f0, f1)) 
		return (True, f)

class MosesCaptureRectified(Camera):
	cameras = []

	fovX = math.radians(90)
	fovY = math.radians(90)

	resX = 2000
	resY = 2000

	fX = resX/2
	fY = resY/2

	cX = resX/2
	cY = resY/2

	mat = np.array([[fX, 0   , cX, 0],
									[0,    fY, cY, 0],
									[0,    0 ,  1, 0]])

	lx = None
	ly = None
	rx = None
	ry = None
	def __init__(self,indices=INDEX_FROM_JSON, expval = 57, manexp = False, calibration = None, fov = 90, res = 1000, swap=False):
		if calibration is None:
			raise ("Why the hell did you use me if you weren't gonna give me a calibration?")
		self._calibration = dict(calibration) 
		calibration = self._calibration

		if indices == INDEX_FROM_JSON:
			j = json.load(open("cameras.json"))
			indices = [j["leftIndex"],j["rightIndex"]]
		
		if indices == INDEX_TRY_ALL:
			indices = range(10)
			


		# weird things happen with object returned by np.load if you do multiprocessing, and I think it's slower
		# neither of those are concerns but in the habit of doing
		self.swap = swap
		self.fov = math.radians(fov)
		self.res = res
		print(self.fov)
		fov = self.fov
		res = self.res # stop mistakes

		self.focald = (res/2)/math.tan(self.fov/2) # focal length x,y

		self.center = res/2

		self.mat = np.array([[self.focald, 0   , self.center, 0],
												 [0,    self.focald, self.center, 0],
												 [0,    0   ,      1, 0]])
		berp = (self.res,self.res)

		(self.lx, self.ly) = cv2.initUndistortRectifyMap(calibration['leftCameraMatrix'], calibration['leftDistCoeffs'], calibration['R1'],self.mat,berp,m1type = cv2.CV_32FC1)
		(self.rx, self.ry) = cv2.initUndistortRectifyMap(calibration['rightCameraMatrix'], calibration['rightDistCoeffs'], calibration['R2'],self.mat,berp,m1type = cv2.CV_32FC1)


		for idx in indices:
			#Start capturing images for calibration
			cap = cv2.VideoCapture(idx, cv2.CAP_V4L2)
			cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
			cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
			cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
			cap.set(cv2.CAP_PROP_FPS,350)
			cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0 if manexp else 3.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
			# 70 was too low, power frequency lines.
			cap.set(cv2.CAP_PROP_EXPOSURE, expval)
			ret,frame = cap.read()
			print(ret, idx)
			if ret == True:
				self.cameras.append(cap)
	@property
	def calibration(self):
		return self._calibration
	@property
	def resolution(self):
		return (1000,2000)

	def read(self):
		retL, fL  = self.cameras[0].read()
		retR, fR = self.cameras[1].read()
		retL, fL  = self.cameras[0].read()
		retR, fR = self.cameras[1].read()
		while ((retL == False) or (retR == False)):
			print("no")
			retL, fL  = self.cameras[0].read()
			retR, fR = self.cameras[1].read()
		if self.swap:
			print("sw")
			e = fL
			fL = fR
			fR = e
			del e
		fL = cv2.remap(fL,self.lx,self.ly,cv2.INTER_LINEAR)
		fR = cv2.remap(fR,self.rx,self.ry,cv2.INTER_LINEAR)
		f = np.hstack((fL, fR)) 
		return (True, f)
	def readCapture(self):
		return self.read()

	

	def release(self):
		for ele in self.cameras:
			ele.release()



class CV2Camera(Camera):

	def __init__(self, index, frameHeight, frameWidth):
		self._frameHeight = frameHeight
		self._frameWidth = frameWidth
		self._cap = cv2.VideoCapture(index)
		self._cap.set(cv2.CAP_PROP_FRAME_WIDTH , self._frameWidth)
		self._cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self._frameHeight)
		return

	def readCapture(self):
		return self._cap.read()

	def release(self):
		self._cap.release()
		return

	@property
	def resolution(self):
		return (self._frameHeight, self._frameWidth)

	@property
	def calibration(self):
		return np.load("./cameraCalibration_cv2.npz", ) #TODO consider read always vs init

	def readNewFrame(self, waitTime=0.1, tryCount=10):
		return cv2.cvtColor(Camera.readNewFrame(self, waitTime, tryCount), cv2.COLOR_BGR2GRAY)

class ZedCamera(CV2Camera):

	@property
	def calibration(self):
		return np.load("./cameraCalibration_cv2_ZED.npz", )

class T265Camera(Camera): #can be potentially refactored to generic realsense camera however everything in intelutils is probably hardcoded for t265

	def __init__(self):
		import pyrealsense2 as rs2
		import intelutils
		self._cap = intelutils.intelCamThread(frame_callback = lambda frame: None)
		self._cap.start()
		self._frameWidth = 848 * 2
		self._frameHeight = 800
		return

	def readCapture(self):
		return self._cap.read()

	def release(self):
		self._cap.kill()
		return

	@property
	def calibration(self):
		return np.load("./cameraCalibration_rs.npz", ) #TODO consider read always vs init

	def readNewFrame(self, waitTime=0.1, tryCount=10):
		self.readCapture() #reseting newFrame flag in intelutils
		return Camera.readNewFrame(self, waitTime, tryCount)

	@property
	def resolution(self):
		return (self._frameHeight, self._frameWidth)

	def fisheye(self):
		return True

class LeapMotion(Camera):

	def __init__(self, index, frameHeight, frameWidth):
		import leapuvc
		self._frameHeight = frameHeight
		self._frameWidth = frameWidth
		self._cap = leapuvc.leapImageThread(index, resolution=(self._frameWidth >> 1, self._frameHeight))
		self._cap.start()
		return

	def readCapture(self):
		ret, frame = self._cap.read()
		if frame is not None:
			frame = np.hstack(frame[:2])
		return ret, frame

	def release(self):
		self._cap.timeout = -1
		return

	@property
	def calibration(self):
		return np.load("./cameraCalibration_leap.npz", ) #TODO consider read always vs init

	def readNewFrame(self, waitTime=0.1, tryCount=10):
		self.readCapture() #reseting newFrame flag in intelutils
		return Camera.readNewFrame(self, waitTime, tryCount)

	@property
	def resolution(self):
		return (self._frameHeight, self._frameWidth)