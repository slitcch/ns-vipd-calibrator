import time
import cv2
import cv2.aruco
import numpy as np
import random
import multiprocessing
import traceback
import sys
import glob
import argparse
import time

parser = argparse.ArgumentParser()
parser.add_argument('ipd', help="ipd you think this is")
args = parser.parse_args()
capturefolder = f"cache/{args.ipd}/cameracal/"



np.set_printoptions(suppress = True, precision=8)

# squaresize = .0551
# squaresize = .05178
squaresize = 0.04105
# squaresize = .20454/5 # .040908
arucosize = squaresize*.8

checkerboardWidth = 12
checkerboardHeight = 8

monitorWidthPx = 2560
monitorHeightPx = 1440
bine = 15

numImagesRequired = (checkerboardWidth-1)*(checkerboardHeight-1)


dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_100)
board = cv2.aruco.CharucoBoard_create(checkerboardWidth,checkerboardHeight,squaresize,arucosize,dictionary)

count = -1
imgsIndiv = {"left": [], "right": []}

imgsStereo = {"left": [], "right": []}
nonsense = 0

while True:
	count += 1; # starts at zero, just didn't want to forget
	# try:
	l = cv2.imread(f"{capturefolder}/left{count}.png")
	r = cv2.imread(f"{capturefolder}/right{count}.png")
	if l is None or r is None:
		print(f"couldn't load images {count}...")
		nonsense += 1
	else:
		imgsIndiv['left'].append(l)
		imgsIndiv['right'].append(r)
	if l is None and r is None:
		break


print("okay, calbrating....")
man = multiprocessing.Manager()
yeah = man.dict()

flags = (cv2.CALIB_RATIONAL_MODEL) #cv2.CALIB_THIN_PRISM_MODEL+
# (cv2.CALIB_USE_INTRINSIC_GUESS  + cv2.CALIB_FIX_ASPECT_RATIO + cv2.CALIB_RATIONAL_MODEL) not mono or stereo
# 
criteria = (cv2.TERM_CRITERIA_EPS & cv2.TERM_CRITERIA_COUNT, 10000, 1e-9)

def multiprocess_individual(listOfImgs,dictOut,imsize):
	allCharucoCorners = []
	allCharucoIds = []
	for img in listOfImgs:
		arucoCorners, arucoIds, _ = cv2.aruco.detectMarkers(img,dictionary)
		if len(arucoCorners)>0:
			retval, charucoCorners, charucoIds = cv2.aruco.interpolateCornersCharuco(arucoCorners, arucoIds ,img,board,)
			if (len(arucoCorners) > 5):
				allCharucoIds.append(charucoIds)
				allCharucoCorners.append(charucoCorners)

	# calibrateCameraCharuco has some assert()s in it, I think as sanity checks to make sure the math hasn't gone horribly wrong.
	# These asserts will raise an exception though, so catch it
	try:
		cameraMatrixInit = np.array([[ 1000.,    0., imsize[0]/2.],
		                    [    0., 1000., imsize[1]/2.],
		                    [    0.,    0.,           1.]])

		distCoeffsInit = np.zeros((5,1))
		
		(retval, cameraMatrix, distCoeffs, rvecs, 
		tvecs) = cv2.aruco.calibrateCameraCharuco(
			charucoCorners = allCharucoCorners, charucoIds = allCharucoIds, board = board,imageSize = imsize,cameraMatrix=cameraMatrixInit,distCoeffs=distCoeffsInit ,criteria = criteria, flags=flags)
		yeah[dictOut] = {"cameraMatrix": cameraMatrix, "distCoeffs": distCoeffs, "rvecs": rvecs, "tvecs": tvecs, "charucoCorners": allCharucoCorners, "charucoIds": allCharucoIds}
		print(yeah[dictOut]['distCoeffs'])


	except:
		traceback.print_exc(file=sys.stdout)
		print("oh no", dictOut)



nobs = []
nobs.append(multiprocessing.Process(target=multiprocess_individual, args=(imgsIndiv['left'], "left",(1280,720))))
nobs.append(multiprocessing.Process(target=multiprocess_individual, args=(imgsIndiv['right'], "right",(1280,720))))
for job in nobs:
	job.start()
jobs = nobs
for nob in jobs:
	nob.join()

# Chessboard parameters
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ...., (checkerboardWidth, checkerboardHeight,0)
objPtsIdealFrame = np.zeros(((checkerboardHeight-1)*(checkerboardWidth-1),3), np.float32)
objPtsIdealFrame[:,:2] = np.mgrid[0:checkerboardWidth-1,0:checkerboardHeight-1].T.reshape(-1,2)
objPtsIdealFrame = objPtsIdealFrame * squaresize # Set the Object Points to be in real coordinates
objPtsIdealFrame = np.asarray([objPtsIdealFrame])

# objp = np.copy(objpp)
# for x in range(3):
# 	objp = np.concatenate((objp, objpp), axis=0)
# # print(objp)

# objp = objp
# print(objp)

stereoLeftCorners = []
stereoLeftIds = []
stereoRightCorners = []
stereoRightIds = []
objectPointsListPerFrame = []

for leftCorners, leftIds, rightCorners, rightIds in zip(yeah['left']['charucoCorners'], yeah['left']['charucoIds'], yeah['right']['charucoCorners'], yeah['right']['charucoIds']):
	lI = [ele[0] for ele in leftIds]
	rI = [ele[0] for ele in rightIds]

	finalLeftCorners = []
	finalRightCorners = []
	finalObjPts = []

	cI = [ele[0] for ele in leftIds if ele in rightIds]

	for leftId, leftCorner in zip(lI,leftCorners):
		if leftId in cI:
			finalLeftCorners.append(leftCorner)
	
	for rightId, rightCorner in zip(rI,rightCorners):
		if rightId in cI:
			finalRightCorners.append(rightCorner)

	for Id, pt in zip(range(77),objPtsIdealFrame[0]):
		if Id in cI:
			finalObjPts.append(pt)
			# print("pt", Id, pt)
	stereoLeftCorners.append(np.array(finalLeftCorners))
	objectPointsListPerFrame.append(np.array(finalObjPts))
	stereoRightCorners.append(np.array(finalRightCorners))
	

# print(objp)

objectPointsListPerFrame = np.array(objectPointsListPerFrame)
# print(objectPointsListPerFrame)

# stereoLeftCorners = np.array(stereoLeftCorners)

print(len(objectPointsListPerFrame[0]), len(stereoLeftCorners[0]))




print("hello", len(objPtsIdealFrame))
imsize = (1280,720)

flagsStereoCalibrate = flags+(cv2.CALIB_FIX_INTRINSIC)

valid, leftCameraMatrix, leftDistCoeffs, rightCameraMatrix, rightDistCoeffs, leftToRightRot, leftToRightTrans, essentialMat, fundamentalMat = (
 							cv2.stereoCalibrate(objectPointsListPerFrame, stereoLeftCorners, stereoRightCorners, yeah["left"]["cameraMatrix"], yeah["left"]["distCoeffs"], yeah["right"]["cameraMatrix"], yeah["right"]["distCoeffs"], (1280,720),criteria=criteria, flags=flags))
print("here \n\n\n\\n\n\n", valid, "\n\n\n\n")
if(valid):
	# Construct the stereo-rectified parameters for display
	R1, R2, P1, P2, Q, validPixROI1, validPixROI2 = cv2.stereoRectify(leftCameraMatrix,  leftDistCoeffs, 
																	  rightCameraMatrix, rightDistCoeffs, 
																	 imsize, 
																	  leftToRightRot, leftToRightTrans,flags=flagsStereoCalibrate)

	leftUndistortMap = [None, None]
	leftUndistortMap[0], leftUndistortMap[1] = cv2.initUndistortRectifyMap(leftCameraMatrix, leftDistCoeffs, 
																		   R1, P1, imsize, cv2.CV_32FC1)
	rightUndistortMap = [None, None]
	rightUndistortMap[0], rightUndistortMap[1] = cv2.initUndistortRectifyMap(rightCameraMatrix, rightDistCoeffs, 
																			 R2, P2, imsize, cv2.CV_32FC1)
	undistortMap = (leftUndistortMap, rightUndistortMap)

	print("Stereo Calibration Completed!")
	print("Left to Right Rotation Matrix:")
	print(leftToRightRot)
	print("Left to Right Translation:")
	print(leftToRightTrans)
	print("Essential Matrix:")
	print(essentialMat)
	print("Fundamental Matrix:")
	print(fundamentalMat)
	np.savez(f"cache/{args.ipd}/cameraCalibration_cv2.npz",
			leftCameraMatrix=leftCameraMatrix,
			rightCameraMatrix=rightCameraMatrix,
			leftDistCoeffs=leftDistCoeffs,
			rightDistCoeffs=rightDistCoeffs,
			leftToRightTrans=leftToRightTrans,
			leftToRightRot=leftToRightRot,
			R1=R1,
			R2=R2,
			P1=P1,
			P2=P2,
			baseline=float(leftToRightTrans[0]*-1.0)
			)
	
	# np.savez("cameraCalibration_cv2.npz",
	# 		leftCameraMatrix=yeah['left']['cameraMatrix'],
	# 		rightCameraMatrix=yeah['right']['cameraMatrix'],
	# 		leftDistCoeffs=yeah['left']['distCoeffs'],
	# 		rightDistCoeffs=yeah['right']['distCoeffs'],
	# 		leftToRightTrans=leftToRightTrans,
	# 		leftToRightRot=leftToRightRot,
	# 		R1=R1,
	# 		R2=R2,
	# 		P1=P1,
	# 		P2=P2,
	# 		baseline=float(leftToRightTrans[0]*-1.0)
	# 		)

	import datetime
	import shutil
	timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
	shutil.copy(f"cache/{args.ipd}/cameraCalibration_cv2.npz", f"cache/{args.ipd}/cameraCalibration_cv2-{timea}.npz")


print(dict(np.load(f"cache/{args.ipd}/cameraCalibration_cv2.npz")))

	
# np.savez("cameraCalibration_cv2.npz",
# 		leftCameraMatrix=yeah['left']['cameraMatrix'],
# 		rightCameraMatrix=yeah['right']['cameraMatrix'],
# 		leftDistCoeffs=yeah['left']['distCoeffs'],
# 		rightDistCoeffs=yeah['right']['distCoeffs'],
# 		)
# import datetime
# import shutil
# timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
# shutil.copy("cameraCalibration_cv2.npz", f"cameraCalibration_cv2-{timea}.npz")

# exit(0)

# np.savez("cameraCalibration_cv2.npz",
# 		leftCameraMatrix=yeah['left']['cameraMatrix'],
# 		rightCameraMatrix=yeah['right']['cameraMatrix'],
# 		leftDistCoeffs=yeah['left']['distCoeffs'],
# 		rightDistCoeffs=yeah['right']['distCoeffs'],
# 		)
# import datetime
# import shutil
# timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
# shutil.copy("cameraCalibration_cv2.npz", f"cameraCalibration_cv2-{timea}.npz")

# exit(0)

