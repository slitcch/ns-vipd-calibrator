import multiprocessing
import queue
import sys

# import cv2, cvui
import numpy as np
# import pyrealsense2 as rs
import pdb
import os
import simplejson as json
import decimal	
import time
from decimal import *
import pickle

getcontext().prec = 3

#Moses: 2D surface. X is input, Y is input, and the Z output is the output X axis.
# Fit the multidimensional polynomials

#Alternative input, for me:
#Get list of valid grid points. So, [ [Uin0,Vin0,rectXout0,rectYout0], [Uin1,Vin2,rectXout3,rectYout4], ... [Uinn,Vinn,rectXoutn,rectYoutn]  ] (expect n to be less than 4225, grid size is 65x65).
#The above would be an AoS of course. Conceptually that makes more sense to me, but the function takes a SoA, which would look like this:
#Uin = [Uin0,Uin1,Uin2, ... Uin_n]
#Vin = [Vin0,Vin1,Vin2, ... Vin_n]
#rectXout = [rectXout_0,rectXout_1,rectXout_2, ... rectXout_n]
#rectYout = [rectYout_0,rectYout_1,rectYout_2, ... rectYout_n]
#List slicing makes it pretty easy to convert these back and forth.
#Anyway.
# x_coeffs = polyfit2d(Uin, Vin, rectXout, np.asarray([3,3]))
# y_coeffs = polyfit2d(Uin, Vin, rectYout, np.asarray([3,3]))
#1st var, "input" x axis: 

def polyfit2d(X, Y, Z, deg):
	"""Fit a 3D polynomial of degree deg"""
	# print(f"X is {X}, Y is {Y}, Z is {Z}, deg is {deg}")
	vander = np.polynomial.polynomial.polyvander2d(X, Y, deg)
	#print(f"Initially, vander is {vander}")
	vander = vander.reshape((-1, vander.shape[-1]))
	c      = np.linalg.lstsq(vander, Z.reshape((vander.shape[0],)), rcond=-1)[0]
	c = c.reshape(deg + 1)
	return c

def polyval2d(X, Y, C):
	"""Evaluate a 2D Polynomial with coefficients C"""
	output = np.zeros_like(X)
	for i in range(C.shape[0]):
		for j in range(C.shape[1]):
			output += C[i, j] * (X ** i) * (Y ** j)
	return output

def polyval2dExpanded(X, Y, C):
	"""Evaluate a 2D Polynomial with coefficients C, but more verbose"""
	Cf = C.flatten()
	X2 = X * X; X3 = X2 * X
	Y2 = Y * Y; Y3 = Y2 * Y
	return (((Cf[ 0]     ) + (Cf[ 1]      * Y) + (Cf[ 2]      * Y2) + (Cf[ 3]      * Y3)) +
					((Cf[ 4] * X ) + (Cf[ 5] * X  * Y) + (Cf[ 6] * X  * Y2) + (Cf[ 7] * X  * Y3)) +
					((Cf[ 8] * X2) + (Cf[ 9] * X2 * Y) + (Cf[10] * X2 * Y2) + (Cf[11] * X2 * Y3)) +
					((Cf[12] * X3) + (Cf[13] * X3 * Y) + (Cf[14] * X3 * Y2) + (Cf[15] * X3 * Y3)))



def print_coeffs(coeffs):
	"""Print Coefficients to the console for use in other languages (this is GLSL)"""
	global counter
	flattened_coeffs = coeffs.flatten()
	num_coeffs = flattened_coeffs.shape[0]
	coeff_string = "float[] "+names[counter]+"  = float["+str(num_coeffs)+"]( "
	for i in range(num_coeffs):
		coeff_string += str(flattened_coeffs[i]) + (", " if i < num_coeffs-1 else "")
	coeff_string += ");"
	#print(coeff_string)

	# Also write these coefficients to the calibration dict
	outputCalibration[names[counter]] = flattened_coeffs.tolist()

	counter += 1




def it_is_my_time(): # hopefully, quit from any context
	pipe.stop()
	exit(0)

def generate_rectilinear_points():
	x_locs = np.arange(-4,4,0.1)
	y_locs = np.arange(-4,4,0.1)
	arr = []
	for x_loc in x_locs:
		for y_loc in y_locs:
			arr.append([x_loc, y_loc])
	
	return np.asanyarray([arr])

with open('cache/grids.pickle', 'rb') as f:
	# The protocol version used is detected automatically, so we do not
	# have to specify it.
	grids = pickle.load(f)

elestolaplace = {
	54: {"left": [], "right": []},
	64: {"left": [], "right": []},
	74: {"left": [], "right": []},
}

polys = {
	54: {"left_uv_to_rect_x": None, "left_uv_to_rect_y": None, "right_uv_to_rect_x": None, "right_uv_to_rect_y": None},
	64: {"left_uv_to_rect_x": None, "left_uv_to_rect_y": None, "right_uv_to_rect_x": None, "right_uv_to_rect_y": None},
	74: {"left_uv_to_rect_x": None, "left_uv_to_rect_y": None, "right_uv_to_rect_x": None, "right_uv_to_rect_y": None},
}


for ipd in [64]:
	for side in ["left", "right"]:
		Uin = []
		Vin = [] 
		Xrectout= [] 
		Yrectout = []



		for v in range(65):
			v_num = v/64
			for u in range(65):
				u_num = u/64
				if grids[ipd][side][v][u+1] != None:
					Uin.append(u_num)
					Vin.append(v_num)
					Xrectout.append(grids[ipd][side][v][u+1][0])
					Yrectout.append(grids[ipd][side][v][u+1][1]) #should be Y axis of [u][v]'th element
				else:
					if v > 1 and v < 64 and u+1 > 1 and u+1 < 64:
						elestolaplace[ipd][side].append([v,u+1])

		#print(Xrectout[:20])
		#print(Yrectout[:20])

		Uin = np.asarray(Uin)
		Vin = np.asarray(Vin)
		Xrectout = np.asarray(Xrectout)
		Yrectout = np.asarray(Yrectout)

		x_coeffs = polyfit2d(Uin,        # "Input"  X Axis
												Vin,        # "Input"  Y Axis
												Xrectout, # "Output" X Axis
												np.asarray([3,3]))

		y_coeffs = polyfit2d(Uin,        # "Input"  X Axis
												Vin,        # "Input"  Y Axis
												Yrectout, # "Output" Y Axis
												np.asarray([3,3]))
		np.set_printoptions(suppress=True)

		polys[ipd][f"{side}_uv_to_rect_x"] = list(x_coeffs.flatten())
		polys[ipd][f"{side}_uv_to_rect_y"] = list(y_coeffs.flatten())


		for v in range(65):
			v_num = v/64
			for u in range(65):
				u_num = u/64
				if grids[ipd][side][v][u+1] == None:	
					#print("isNone")
					grids[ipd][side][v][u+1] = [polyval2d(u_num,v_num,x_coeffs), polyval2d(u_num,v_num,y_coeffs)]

		#print(elestolaplace)

		# laplacian smooth everything uncaptured, a whole lot of iterations.
		# we can do this, because we don't attempt to smooth the edges of the grid.
		for it in range(100):
			for v, u in elestolaplace[ipd][side]:
				print(v,u	)
				neighborU = grids[ipd][side][v-1][u]
				neighborD = grids[ipd][side][v+1][u]
				neighborL = grids[ipd][side][v][u-1]
				neighborR = grids[ipd][side][v][u+1]
				print(neighborL, neighborR)
				grids[ipd][side][v][u][0] = (neighborU[0]+neighborD[0]+neighborR[0]+neighborL[0])/4
				grids[ipd][side][v][u][1] = (neighborU[1]+neighborD[1]+neighborR[1]+neighborL[1])/4
		
		# xvel = (xvel + (timestep * (ydir_to_top * k*( real_length_top - target_length )) ) ) * damping

		'''# spring stuff
		for it in range(100):
			for v, u in elestolaplace[ipd][side]
				u = u+1

				k = 0.1 # spring constant
		print(list())
		print(list(y_coeffs.flatten()))nt

				tL = 0.02 # target Length

				
				UfcX = 0 # Up force component X
				UfcY = 0
				if (v > 0):
					UfcX = k * ( np.linalg.norm(np.asarray(grids[ipd][side][v-1][u])) - tL )

				neighborD = grids[ipd][side][v+1][u]
				neighborL = grids[ipd][side][v][u-1]
				neighborR = grids[ipd][side][v][u+1]'''



		

		# laplacian smooth everything, only one iteration.
		# point of this is to remove slight wiggliness caused by the t265's low resolution
		for it in range(1):
			for v in range(1,64):
				for u in range(1,64):
					u = u+1
					neighborU = grids[ipd][side][v-1][u]
					neighborD = grids[ipd][side][v+1][u]
					neighborL = grids[ipd][side][v][u-1]
					neighborR = grids[ipd][side][v][u+1]
					grids[ipd][side][v][u][0] = (neighborU[0]+neighborD[0]+neighborR[0]+neighborL[0])/4
					grids[ipd][side][v][u][1] = (neighborU[1]+neighborD[1]+neighborR[1]+neighborL[1])/4



		for v in range(65):
			for u in range(65):
				for index in [0,1]:
					fstring = "{:.6f}"
					grids[ipd][side][v][u+1][index] = decimal.Decimal(fstring.format(grids[ipd][side][v][u+1][index]))
		print(".",end = "",flush=True)

	with open(f'cache/NSCalPolynomial{ipd}.json', 'w') as outfile:
		ourjson = polys[ipd]
		ourjson["baseline"] = ipd
		ourjson["leftEyeAngleLeft"]   = -0.6
		ourjson["leftEyeAngleRight"]  =  0.6
		ourjson["leftEyeAngleUp"]     =  0.6
		ourjson["leftEyeAngleDown"]   = -0.6

		ourjson["rightEyeAngleLeft"]  = -0.6
		ourjson["rightEyeAngleRight"] =  0.6
		ourjson["rightEyeAngleUp"]    =  0.6
		ourjson["rightEyeAngleDown"]  = -0.6
		s = json.dumps(ourjson,use_decimal=True, indent=2)
		outfile.write(s)


grids[54] = grids[64]
grids[74] = grids[64]

configfilejson = {
					"calibration-type":
					"Moses's multi-ipd calibration",
					"baseline": 0.064,

						"head_tracker_to_eyes_center": {
							"translation_meters": {
								"x": 0,
								"y": -0.066,
								"z": 0.066
							},
							"rotation_quaternion": {
								"x": 0.102931,
								"y": 0,
								"z": 0,
								"w": 0.994689
							}
						},

						"eyes_center_to_hand_tracker": {
							"translation_meters": {
								"x": 0,
								"y": 0.0928,
								"z": -0.066
							},
							"rotation_quaternion": {
								"x": -0.102931,
								"y": 0,
								"z": 0,
								"w": 0.994689
							}
						},
					"grids": grids
}
	

with open('cache/NSCalVIPD.json', 'w') as outfile:
	s = json.dumps(configfilejson,use_decimal=True)
	s = s.replace("]],","]],\n")
	#s = s.replace("[[[","[\n[[")
	#s = s.replace("0.0,","0.000000,")
	#s = s.replace("0.0]","0.000000]")
	s = s.replace("}, \"", "},\n\"")
	s = s.replace(":", ":\n")
	s = s.replace(" -0.","-0.")
	s = s.replace("[0.","[ 0.")
	s = s.replace("999999.000000", "999999")
	s = s.replace("\n [[", "\n[[")
	outfile.write(s)

import datetime
import shutil
timea = datetime.datetime.now().strftime("%y-%m-%d_%H-%M")
shutil.copy("cache/NSCalVIPD.json", f"cache/NSCalVIPD-{timea}.json")



exit(0)
