import time
import cv2
import cv2.aruco
import numpy as np
import time
import traceback
import sys
import math
import cameras

import argparse



parser = argparse.ArgumentParser()
parser.add_argument('ipd', help="ipd you think this is")
args = parser.parse_args()

np.set_printoptions(suppress = True, precision=8)

cv2.namedWindow("frame",0) # 0 at end makes it resizeable in Python 

calibration = np.load(f"cache/{args.ipd}/cameraCalibration_cv2.npz")


dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_APRILTAG_36H11)

cap = cameras.MosesCaptureStereo(indices=cameras.INDEX_FROM_JSON, manexp = False)


while True:
	ret, combinedFrame = cap.read()
	frames = {"left": combinedFrame[:, :int(combinedFrame.shape[1]/2)], "right": combinedFrame[:, int(combinedFrame.shape[1]/2):]}
	out = {}
	for name, frame in frames.items():

		# frame = np.clip( frame.astype(np.float) * mulgrid,0,255)

		frame = frame.astype(np.uint8)
		gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
		arucoCorners, arucoIds, _ = cv2.aruco.detectMarkers(gray,dictionary)
		cv2.aruco.drawDetectedMarkers(frame,arucoCorners,arucoIds)

		out[name] = frame
	# sf = .7
	shw = np.hstack((out['left'],out['right']))
	cv2.imshow(f"frame",shw)
	cv2.waitKey(1)


	

cap.release()
cv2.destroyAllWindows()