import numpy as np        # `pip install numpy`
import cv2                # `pip install opencv-python`
import cv2.aruco as aruco # `pip install opencv-contrib-python`

from cameras import T265Camera, ZedCamera, LeapMotion, MosesCaptureRectified
import cameras

# Initialize ArUco Tracking
aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_100 )
parameters =  aruco.DetectorParameters_create()
parameters.cornerRefinementMethod = cv2.aruco.CORNER_REFINE_APRILTAG

board = aruco.GridBoard_create(3, 3, 0.03, 0.005, aruco_dict, 0)

zed = MosesCaptureRectified(indices=cameras.INDEX_FROM_JSON,calibration=np.load(f"cache/64-1/cameraCalibration_cv2.npz"), manexp = True, expval=100)

# t265 = T265Camera()
maps = [None, None]

lm = LeapMotion(1, 480, 1280)
lm._cap.setExposure(200)
lm._cap.setGain(5)

stereoCameras = (lm,zed)
images = ["left", "right"]

def inversePerspective(rvec, tvec):
	""" Applies perspective transform for given rvec and tvec. """
	R, _ = cv2.Rodrigues(rvec)
	R = np.matrix(R).T
	invTvec = np.dot(R, np.matrix(-tvec))
	invRvec, _ = cv2.Rodrigues(R)
	return invRvec, invTvec

while(True):
	vecs = []
	for ic, camera in enumerate(stereoCameras):
		newFrame, frame = camera.readCapture()
		if(newFrame):
			if len(frame.shape) > 2:
				frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
			if(type(camera).__name__ == "LeapMotion"):
				frame = cv2.flip(frame, -1)
			leftRightImage = np.split(frame, 2, 1)
			ivecs = []
			colorFrames = []
			for ii, image in enumerate(images):

				camera_matrix = camera.calibration[image + "CameraMatrix"]
				dist_coeffs = camera.calibration[image + "DistCoeffs"]

				#TODO
				if(type(camera).__name__ == "T265Camera"):
					if maps[ii] is None:
						maps[ii] = cv2.fisheye.initUndistortRectifyMap(camera_matrix, dist_coeffs, None, camera_matrix, (camera.resolution[1] >> 1, camera.resolution[0]), cv2.CV_32FC1)
					map1, map2 = maps[ii]
					leftRightImage[ii] = cv2.remap(leftRightImage[ii], map1, map2, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT)
					dist_coeffs = np.zeros(dist_coeffs.shape)
				corners, ids, rejectedImgPoints = aruco.detectMarkers(leftRightImage[ii], aruco_dict, parameters=parameters)
				if(corners is not None):
					retval, rvec, tvec = aruco.estimatePoseBoard( corners, ids, board, camera_matrix, dist_coeffs, None, None)
					if rvec is not None and tvec is not None:
						ivecs.append((rvec, tvec))
					# Convert to color for drawing
					colorFrame = cv2.cvtColor(leftRightImage[ii], cv2.COLOR_GRAY2BGR)
					if(retval > 0):
						colorFrame = aruco.drawAxis( colorFrame, camera_matrix, dist_coeffs, rvec, tvec, 0.1 )
					colorFrame = aruco.drawDetectedMarkers(colorFrame, corners, ids)
					colorFrames.append(colorFrame)
			if len(ivecs) == 2:
				vecs.append(ivecs)
			if len(colorFrames) == 2:
				cv2.imshow(type(camera).__name__ + ' Frame', cv2.resize(np.hstack(colorFrames), dsize=(camera.resolution[1] >> 1, camera.resolution[0] >> 1)))#

	waitedKey = cv2.waitKey(1) & 0xFF
	if(waitedKey == ord('q')):
		break
	elif(waitedKey == ord('c')):
		lv = len(vecs)
		print(lv, len(stereoCameras))
		if(lv == len(stereoCameras) and lv > 1):
			for i in range(1, lv):
				print(type(stereoCameras[i]).__name__)
				lrR = []
				lrT = []
				for j in range(2):
					baseRvec, baseTvec = vecs[0][j]
					rvec, tvec = inversePerspective(*vecs[i][j])
					info = cv2.composeRT(rvec, tvec, baseRvec, baseTvec)
					lrR.append(np.array([c[0] for c in np.degrees(info[0])]))
					lrT.append(np.array([c[0] for c in info[1]]))
				print("R:", np.mean(lrR, axis=0))
				print("T:", np.mean(lrT, axis=0), "LR D:", np.linalg.norm(lrT[0] - lrT[1]))

for camera in stereoCameras:
	camera.release()
cv2.destroyAllWindows()
