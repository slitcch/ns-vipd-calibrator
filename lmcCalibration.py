import cv2
import leapuvc
import numpy as np

capResolution = (640, 480)
cam = cv2.VideoCapture(0 + cv2.CAP_DSHOW)
cam.set(cv2.CAP_PROP_FRAME_WIDTH, capResolution[0])
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, capResolution[1])
calibration = leapuvc.retrieveLeapCalibration(cam, capResolution)
ret, frame = cam.read()
if ret is True:
	cv2.imshow("F", frame)
	cv2.waitKey(0)
else:
	print("Something went wrong!")
cam.release()

cal = {}
for i, cam in enumerate(("left", "right")):
	cal[cam + "CameraMatrix"] = calibration[cam]["extrinsics"]["cameraMatrix"]
	cal[cam + "DistCoeffs"] = calibration[cam]["intrinsics"]["distCoeffs"]
print(cal)

np.savez("cameraCalibration_leap.npz",
	leftCameraMatrix=cal["leftCameraMatrix"],
	rightCameraMatrix=cal["rightCameraMatrix"],
	leftDistCoeffs=cal["leftDistCoeffs"],
	rightDistCoeffs=cal["rightDistCoeffs"]
)
