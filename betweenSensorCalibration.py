import numpy as np
import cv2
import cv2.aruco as aruco
import time
import datetime
from tempfile import TemporaryFile
import json
from cameras import ZedCamera, T265Camera

# CONFIGURATION PARAMETERS

# The number of images to capture before beginning calibration
numImagesRequired = 20
# The dimension of a single square on the checkerboard in METERS
checkerboardDimension = 0.027 # This equates to 27 millimeter wide squares
# The number of inside corners on the width (long axis) of the checkerboard
checkerboardWidth = 9
# The number of inside corners on the height (short axis) of the checkerboard
checkerboardHeight = 6

# Initialize some persistent state variables
side = "left"
allLeftCorners = []
allRightCorners = []
lastTime = 0

# Chessboard parameters
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ...., (checkerboardWidth, checkerboardHeight,0)
objpp = np.zeros((checkerboardHeight*checkerboardWidth,3), np.float32)
objpp[:,:2] = np.mgrid[0:checkerboardWidth,0:checkerboardHeight].T.reshape(-1,2)
objpp = objpp * checkerboardDimension # Set the Object Points to be in real coordinates
objpp = np.asarray([objpp])
objp = np.copy(objpp)
for x in range(numImagesRequired-1):
	objp = np.concatenate((objp, objpp), axis=0)

# Termination Criteria for the subpixel corner refinement
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

baseCam = T265Camera()
otherCam = ZedCamera(0, 720, 2560)

leftCameraMatrix = otherCam.calibration[side + "CameraMatrix"]
leftDistCoeffs = otherCam.calibration[side + "DistCoeffs"]
rightCameraMatrix = baseCam.calibration[side + "CameraMatrix"]
rightDistCoeffs = baseCam.calibration[side + "DistCoeffs"]

map1, map2 = cv2.fisheye.initUndistortRectifyMap(rightCameraMatrix, rightDistCoeffs, None, rightCameraMatrix, (848, 800), cv2.CV_32FC1)

while(not (cv2.waitKey(1) & 0xFF == ord('q'))):
	# Capture frame-by-frame
	lret, lframe = otherCam.readCapture()
	rret, rframe = baseCam.readCapture()
	if(lret and rret):
		fi = 0 if side == "left" else 1
		leftFrame = np.split(lframe, 2, axis=1)[fi]
		rightFrame = np.split(rframe, 2, axis=1)[fi]
		rightFrame = cv2.remap(rightFrame, map1, map2, cv2.INTER_LINEAR, cv2.BORDER_CONSTANT)
		# Detect the Chessboard Corners in the Left Image
		gray = cv2.cvtColor(leftFrame, cv2.COLOR_BGR2GRAY)
		leftDetected, corners = cv2.findChessboardCorners(gray, (checkerboardWidth,checkerboardHeight), None, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE + cv2.CALIB_CB_FAST_CHECK)
		if leftDetected:
			leftCorners = cv2.cornerSubPix(gray, corners,(checkerboardWidth,checkerboardHeight),(-1,-1),criteria)
			cv2.drawChessboardCorners(leftFrame, (checkerboardWidth,checkerboardHeight), leftCorners, True)
		cv2.imshow("left", leftFrame)

		# Detect the Chessboard Corners in the Right Image
		gray = rightFrame
		rightDetected, corners = cv2.findChessboardCorners(gray, (checkerboardWidth,checkerboardHeight), None, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE + cv2.CALIB_CB_FAST_CHECK)
		if rightDetected:
			rightCorners = cv2.cornerSubPix(gray, corners,(checkerboardWidth,checkerboardHeight),(-1,-1),criteria)
			cv2.drawChessboardCorners(rightFrame, (checkerboardWidth,checkerboardHeight), rightCorners, True)
		cv2.imshow("right", rightFrame)

		# Add the detected points to our running arrays when the board is detected in both cameras
		if(leftDetected and rightDetected and time.time() - lastTime > 1):
			allLeftCorners.append(leftCorners)
			allRightCorners.append(rightCorners)
			lastTime = time.time()
			print("Added Snapshot to array of points")

		# Once we have all the data we need, begin calibrating!!!
		if(len(allLeftCorners)==numImagesRequired):
			valid, _, _, _, _, leftToRightRot, leftToRightTrans, essentialMat, fundamentalMat = (
				cv2.stereoCalibrate(objp, allLeftCorners, allRightCorners, leftCameraMatrix, np.zeros(leftDistCoeffs.shape), rightCameraMatrix, rightDistCoeffs, None, flags=cv2.CALIB_FIX_INTRINSIC))

			if(valid):
				print("Stereo Calibration Completed!")
				print("Left to Right Rotation Matrix:")
				print(leftToRightRot)
				print("Left to Right Translation:")
				print(leftToRightTrans)
				other = "left" if side == "right" else "right"
				calibration = None
				try:
					calibration = np.load(other + "Calibration.npz")
				except Exception as e:
					print("No valid calibration file found for the other side")
					print(e)
				if calibration is not None:
					leftToRightRotOther = calibration["leftToRightRot"]
					leftToRightTransOther = calibration["leftToRightTrans"]
					rot = (cv2.Rodrigues(leftToRightRotOther)[0].ravel() + cv2.Rodrigues(leftToRightRot)[0].ravel()) * 0.5
					rot = [np.degrees(x) for x in rot]
					trans = (leftToRightTransOther.ravel() + leftToRightTrans.ravel()) * 0.5
					print("R:", rot)
					print("T:", trans)
				else:
					np.savez(side + "Calibration.npz",
						leftToRightRot=leftToRightRot,
						leftToRightTrans=leftToRightTrans
					)
			else:
				print("stereoCalibrate failed!!!")
			break

# When everything is done, release the capture
cv2.destroyAllWindows()
baseCam.release()
otherCam.release()
