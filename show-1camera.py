import time
import cv2
import cv2.aruco
import numpy as np
import time
import traceback
import sys
import math
import cameras

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('ipd', help="ipd you think this is")
args = parser.parse_args()

np.set_printoptions(suppress = True, precision=8)

cv2.namedWindow("frame",cv2.WINDOW_AUTOSIZE) # 0 at end makes it resizeable in Python 

calibration = np.load(f"cache/{args.ipd}/cameraCalibration_cv2.npz")
print(dict(calibration))

fovX = math.radians(128)
fovY = math.radians(72)

resX = 1280
resY = 720

fX = (resX/2)/math.tan(fovX/2)
fY = (resY/2)/math.tan(fovY/2)
fX = 450
fY = fX

print(fX,fY)

cX = resX/2
cY = resY/2

mat = np.array([[fX, 0   , cX, 0],
								[0,    fY, cY, 0],
								[0,    0 ,  1, 0]])

lx = None
ly = None
rx = None
ry = None
calibration = dict(calibration) 


indices = range(10)
			


# weird things happen with object returned by np.load if you do multiprocessing, and I think it's slower
# neither of those are concerns but in the habit of doing
# fov = math.radians(90)
# res = 1000 # stop mistakes

# focald = (res/2)/math.tan(fov/2) # focal length x,y

# center = res/2

# mat = np.array([[focald, 0   ,   center, 0],
# 							 [0,       focald, center, 0],
# 							 [0,        0   ,      1, 0]])
berp = (resX,resY)

(lx, ly) = cv2.initUndistortRectifyMap(calibration['leftCameraMatrix'], calibration['leftDistCoeffs'], calibration['R1'],mat,berp,m1type = cv2.CV_32FC1)
(rx, ry) = cv2.initUndistortRectifyMap(calibration['rightCameraMatrix'], calibration['rightDistCoeffs'], calibration['R2'],mat,berp,m1type = cv2.CV_32FC1)

cap = None

manexp = False;

for idx in indices:
	#Start capturing images for calibration
	cap = cv2.VideoCapture(idx, cv2.CAP_V4L2)
	cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc("M","J","P","G"))
	cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
	cap.set(cv2.CAP_PROP_FPS,350)
	cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1.0 if manexp else 3.0) # 1.0 = manual exposure, 3.0 = auto exposure. I got this by messing with settings in OBS, then doing cap.get() to find the value
	# 70 was too low, power frequency lines.
	# cap.set(cv2.CAP_PROP_EXPOSURE, expval)
	ret,frame = cap.read()
	print(ret, idx)
	if ret == True:
		break

	# def read():
	# 	retL, fL  = cap.read()
	# 	while ((retL == False) or (retR == False)):
	# 		print("no")
	# 		retL, fL  = self.cameras[0].read()
	# 		retR, fR = self.cameras[1].read()
	# 	if self.swap:
	# 		print("sw")
	# 		e = fL
	# 		fL = fR
	# 		fR = e
	# 		del e
	# 	fL = cv2.remap(fL,self.lx,self.ly,cv2.INTER_LINEAR)
	# 	fR = cv2.remap(fR,self.rx,self.ry,cv2.INTER_LINEAR)
	# 	f = np.hstack((fL, fR)) 
	# 	return (True, f)
	# def readCapture(self):
	# 	return self.read()

	

	# def release(self):
	# 	for ele in self.cameras:
	# 		ele.release()



while True:
	ret, frame = cap.read()
	if ret:
		fL = cv2.remap(frame,lx,ly,cv2.INTER_LINEAR)
		cv2.imshow(f"frame",frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break
	

cap.release()
cv2.destroyAllWindows()